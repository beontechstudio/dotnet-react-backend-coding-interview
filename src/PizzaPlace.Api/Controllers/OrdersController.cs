﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PizzaPlace.Api.Models;
using PizzaPlace.Api.Utils;
using PizzaPlace.Core.Entities;
using PizzaPlace.Core.Interfaces;

namespace PizzaPlace.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;

        public OrdersController(IOrderService orderService, IMapper mapper)
        {
            _orderService = orderService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var ordersResult = await _orderService.GetOrdersAsync();
            if (!ordersResult.Succeeded)
            {
                return new ErrorResponseActionResult
                {
                    Result = new ErrorResponse
                    {
                        Error = ordersResult.Error,
                    },
                };
            }

            return Ok(_mapper.Map<IEnumerable<OrderDataTransferObject>>(ordersResult.Result));
        }
    }
}
