﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using PizzaPlace.Api.Models;
using PizzaPlace.Api.Utils;
using PizzaPlace.Core.Entities;
using PizzaPlace.Core.Interfaces;

namespace PizzaPlace.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PizzasController : ControllerBase
    {
        private readonly IPizzaService _pizzaService;
        private readonly IMapper _mapper;

        public PizzasController(IPizzaService pizzaService, IMapper mapper)
        {
            _pizzaService = pizzaService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var pizzasResult = await _pizzaService.GetAllAsync();
            if (!pizzasResult.Succeeded)
            {
                return new ErrorResponseActionResult
                {
                    Result = new ErrorResponse
                    {
                        Error = pizzasResult.Error,
                    },
                };
            }
            return Ok(_mapper.Map<IEnumerable<PizzaDataTransferObject>>(pizzasResult.Result));
        }

        [HttpPost("{pizzaId}/enable")]
        public async Task<IActionResult> Enable(int pizzaId)
        {
            var updateResult = await _pizzaService.UpdateStatusAsync(pizzaId, true);
            if (!updateResult.Succeeded)
            {
                return new ErrorResponseActionResult
                {
                    Result = new ErrorResponse
                    {
                        Error = updateResult.Error,
                    },
                };
            }
            return Ok(_mapper.Map<PizzaDataTransferObject>(updateResult.Result));
        }

        [HttpPost("{pizzaId}/disable")]
        public async Task<IActionResult> Disable(int pizzaId)
        {
            var updateResult = await _pizzaService.UpdateStatusAsync(pizzaId, false);
            if (!updateResult.Succeeded)
            {
                return new ErrorResponseActionResult
                {
                    Result = new ErrorResponse
                    {
                        Error = updateResult.Error,
                    },
                };
            }
            return Ok(_mapper.Map<PizzaDataTransferObject>(updateResult.Result));
        }
    }
}
