﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PizzaPlace.Api.Models;
using PizzaPlace.Core.Entities;

namespace PizzaPlace.Api.MappingProfiles
{
    public sealed class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<Order, OrderDataTransferObject>()
                .ReverseMap();
            CreateMap<PizzaOrder, PizzaOrderDataTransferObject>()
                .ForMember(dest => dest.PizzaName,
                    opt => opt.MapFrom(src => src.Pizza.Name))
                .ForMember(dest => dest.BasePrice,
                    opt => opt.MapFrom(src => src.Pizza.BasePrice));

            CreateMap<PizzaOrderDataTransferObject, PizzaOrder>();
        }
    }
}
