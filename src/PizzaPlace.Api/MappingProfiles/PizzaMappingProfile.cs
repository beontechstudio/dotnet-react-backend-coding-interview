﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PizzaPlace.Api.Models;
using PizzaPlace.Core.Entities;

namespace PizzaPlace.Api.MappingProfiles
{
    public sealed class PizzaMappingProfile : Profile
    {
        public PizzaMappingProfile()
        {
            this.CreateMap<Pizza, PizzaDataTransferObject>().ReverseMap();
            this.CreateMap<Ingredient, IngredientDataTransferObject>().ReverseMap();
        }
    }
}
