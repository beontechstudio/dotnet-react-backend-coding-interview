﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PizzaPlace.Core;

namespace PizzaPlace.Api.Models
{
    public class ErrorResponse
    {
        public Error Error { get; set; }
    }
}
