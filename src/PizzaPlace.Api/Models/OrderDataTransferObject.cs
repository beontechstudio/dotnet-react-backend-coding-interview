﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PizzaPlace.Core.Entities;

namespace PizzaPlace.Api.Models
{
    public class OrderDataTransferObject
    {
        public int Id { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public decimal Subtotal { get; set; }

        public decimal Total { get; set; }

        public IEnumerable<PizzaOrderDataTransferObject> Pizzas { get; set; }
    }
}
