﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaPlace.Api.Models
{
    public class PizzaDataTransferObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<IngredientDataTransferObject> BaseIngredients { get; set; }

        public decimal BasePrice { get; set; }

        public bool CheeseCrust { get; set; }
    }
}
