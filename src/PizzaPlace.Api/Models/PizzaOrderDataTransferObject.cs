﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaPlace.Api.Models
{
    public class PizzaOrderDataTransferObject
    {
        public int PizzaId { get; set; }

        public string PizzaName { get; set; }

        public IEnumerable<IngredientDataTransferObject> ExtraIngredients { get; set; }

        public decimal BasePrice { get; set; }

        public decimal ExtrasTotal { get; set; }

        public decimal Total { get; set; }

        public int Quantity { get; set; }
    }
}
