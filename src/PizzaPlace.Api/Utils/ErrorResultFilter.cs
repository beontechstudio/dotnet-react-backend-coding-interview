﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PizzaPlace.Core;

namespace PizzaPlace.Api.Utils
{
    public class ErrorResultFilter : IAsyncResultFilter
    {
        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if (context.Result is ErrorResponseActionResult casted)
            {
                context.Result = new ObjectResult(casted.Result)
                {
                    StatusCode = casted.Result.Error.Code switch
                    {
                        ErrorCode.InternalError => StatusCodes.Status500InternalServerError,
                        ErrorCode.NotFound => StatusCodes.Status404NotFound,
                        _ => StatusCodes.Status400BadRequest
                    },
                };
            }

            await next();
        }
    }
}
