﻿using System.Collections.Generic;

namespace PizzaPlace.Core.Entities
{
    public class Ingredient
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool IsAvailable { get; set; }

        public int Inventory { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }

        public ICollection<PizzaOrder> PizzaOrders { get; set; }
    }
}