﻿using System.Collections.Generic;

namespace PizzaPlace.Core.Entities
{
    public class Order
    {
        public int Id { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public ICollection<PizzaOrder> Pizzas { get; set; }

        public decimal Subtotal { get; set; }

        public decimal Total { get; set; }
    }
}