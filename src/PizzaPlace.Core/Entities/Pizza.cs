﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaPlace.Core.Entities
{
    public class Pizza
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Ingredient> BaseIngredients { get; set; }

        public decimal BasePrice { get; set; }

        public bool CheeseCrust { get; set; }

        public ICollection<PizzaOrder> PizzaOrders { get; set; }

        public bool IsAvailable { get; set; }
    }
}
