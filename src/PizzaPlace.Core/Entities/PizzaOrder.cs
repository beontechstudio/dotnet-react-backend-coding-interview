﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaPlace.Core.Entities
{
    public class PizzaOrder
    {
        public PizzaOrder()
        {
            ExtraIngredients = new HashSet<Ingredient>();
        }

        public int Id { get; set; }

        public Pizza Pizza { get; set; }

        public int PizzaId { get; set; }

        public Order Order { get; set; }

        public int OrderId { get; set; }

        public ICollection<Ingredient> ExtraIngredients { get; set; }

        public decimal ExtrasTotal { get; set; }

        public decimal Total { get; set; }

        public int Quantity { get; set; }
    }
}
