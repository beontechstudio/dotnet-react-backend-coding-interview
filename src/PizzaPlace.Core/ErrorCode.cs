﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaPlace.Core
{
    public enum ErrorCode
    {
        InternalError,
        BadRequest,
        NotFound
    }
}
