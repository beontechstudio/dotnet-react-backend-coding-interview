﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PizzaPlace.Core.Entities;

namespace PizzaPlace.Core.Interfaces
{
    public interface IOrderService
    {
        public Task<OperationResult<IReadOnlyList<Order>>> GetOrdersAsync();

        public Task<OperationResult<Order>> AddOrderAsync(Order order);
    }
}
