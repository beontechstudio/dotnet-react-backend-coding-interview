﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PizzaPlace.Core.Entities;

namespace PizzaPlace.Core.Interfaces
{
    public interface IPizzaService
    {
        Task<OperationResult<IReadOnlyList<Pizza>>> GetAllAsync();

        Task<OperationResult<Pizza>> CreateAsync(Pizza entity);

        Task<OperationResult<Pizza>> FindAsync(int id);

        Task<OperationResult<Pizza>> UpdateStatusAsync(int pizzaId, bool isAvailable);
    }
}
