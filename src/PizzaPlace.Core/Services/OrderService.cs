﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PizzaPlace.Core.Entities;
using PizzaPlace.Core.Interfaces;

namespace PizzaPlace.Core.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task<OperationResult<IReadOnlyList<Order>>> GetOrdersAsync()
        {
            var orders = await _orderRepository.GetOrdersAsync();
            return new OperationResult<IReadOnlyList<Order>>(orders);
        }

        public async Task<OperationResult<Order>> AddOrderAsync(Order order)
        {
            var result = await _orderRepository.AddOrderAsync(order);
            return new OperationResult<Order>(result);
        }
    }
}