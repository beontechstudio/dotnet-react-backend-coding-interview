﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PizzaPlace.Core.Entities;
using PizzaPlace.Core.Interfaces;

namespace PizzaPlace.Core.Services
{
    public class PizzaService : IPizzaService
    {
        private readonly IRepository<Pizza> _repository;

        public PizzaService(IRepository<Pizza> repository)
        {
            _repository = repository;
        }
        public async Task<OperationResult<IReadOnlyList<Pizza>>> GetAllAsync()
        {
            var pizzas = await _repository.GetAllWithRelatedDataAsync(x => x.BaseIngredients);
            return new OperationResult<IReadOnlyList<Pizza>>(pizzas);
        }

        public async Task<OperationResult<Pizza>> CreateAsync(Pizza entity)
        {
            if (string.IsNullOrEmpty(entity.Name))
            {
                return new OperationResult<Pizza>(new Error
                {
                    Code = ErrorCode.BadRequest,
                    Message = "Name is a mandatory field for pizza"
                });
            }

            var result = await _repository.AddAsync(entity);
            return new OperationResult<Pizza>(result);
        }

        public async Task<OperationResult<Pizza>> FindAsync(int id)
        {
            return await _repository.FirstOrDefaultWithRelatedDataAsync(predicate: x => x.Id == id, includes:x => x.BaseIngredients);
        }

        public async Task<OperationResult<Pizza>> UpdateStatusAsync(int id, bool isAvailable)
        {
            var pizza = await _repository.FirstOrDefaultWithRelatedDataAsync(predicate: x => x.Id == id,
                includes: x => x.BaseIngredients);
            if (pizza == null)
            {
                return new OperationResult<Pizza>(new Error
                {
                    Code = ErrorCode.NotFound,
                    Message = $"Pizza with id {id} was not found"
                });
            }

            pizza.IsAvailable = isAvailable;
            var updatedPizza = await _repository.UpdateAsync(pizza);
            return new OperationResult<Pizza>(updatedPizza);
        }
    }
}
