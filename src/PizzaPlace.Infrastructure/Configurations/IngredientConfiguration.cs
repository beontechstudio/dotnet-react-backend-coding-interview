﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PizzaPlace.Core.Entities;

namespace PizzaPlace.Infrastructure.Configurations
{
    public class IngredientConfiguration : IEntityTypeConfiguration<Ingredient>
    {
        public void Configure(EntityTypeBuilder<Ingredient> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasData(new List<Ingredient>
            {
                new Ingredient
                {
                    Id = -1,
                    Name = "chicken",
                    Price = 0.32m,
                    Inventory = 12,
                    IsAvailable = true
                },
                new Ingredient
                {
                    Id = -2,
                    Name = "bacon",
                    Price = 0.45m,
                    Inventory = 27,
                    IsAvailable = true
                },
                new Ingredient
                {
                    Id = -3,
                    Name = "ham",
                    Price = 0.39m,
                    Inventory = 21,
                    IsAvailable = true
                },
                new Ingredient
                {
                    Id = -4,
                    Name = "pepperoni",
                    Price = 0.34m,
                    Inventory = 2,
                    IsAvailable = true
                },
                new Ingredient
                {
                    Id = -5,
                    Name = "corn",
                    Price = 0.28m,
                    Inventory = 14,
                    IsAvailable = true
                },
                new Ingredient
                {
                    Id = -6,
                    Name = "pineapple",
                    Price = 0.31m,
                    Inventory = 0,
                    IsAvailable = false
                },
                new Ingredient
                {
                    Id = -7,
                    Name = "tomato",
                    Price = 0.36m,
                    Inventory = 1,
                    IsAvailable = true
                }
            });
        }
    }
}
