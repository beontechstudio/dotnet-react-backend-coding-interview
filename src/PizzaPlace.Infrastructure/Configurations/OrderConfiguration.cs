﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PizzaPlace.Core.Entities;

namespace PizzaPlace.Infrastructure.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasMany(x => x.Pizzas)
                .WithOne(p => p.Order)
                .HasForeignKey(p => p.OrderId);

            builder.HasData(new List<Order>
            {
                new Order
                {
                    Id = -1,
                    OrderStatus = OrderStatus.Waiting,
                    Subtotal = 8.06m,
                    Total = 9.269m
                }
            });
        }
    }
}
