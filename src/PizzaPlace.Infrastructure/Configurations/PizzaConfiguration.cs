﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PizzaPlace.Core.Entities;

namespace PizzaPlace.Infrastructure.Configurations
{
    public class PizzaConfiguration : IEntityTypeConfiguration<Pizza>
    {
        public void Configure(EntityTypeBuilder<Pizza> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasMany(x => x.BaseIngredients)
                .WithMany(i => i.Pizzas)
                .UsingEntity(
                    rl =>
                    {
                        rl.HasData(
                            new {PizzasId = -1, BaseIngredientsId = -3},
                            new {PizzasId = -2, BaseIngredientsId = -3},
                            new {PizzasId = -3, BaseIngredientsId = -1},
                            new {PizzasId = -3, BaseIngredientsId = -2},
                            new {PizzasId = -3, BaseIngredientsId = -3},
                            new {PizzasId = -3, BaseIngredientsId = -4},
                            new {PizzasId = -3, BaseIngredientsId = -5},
                            new {PizzasId = -3, BaseIngredientsId = -6},
                            new {PizzasId = -3, BaseIngredientsId = -7});
                    });

            builder.HasData(new List<Pizza>
            {
                new Pizza
                {
                    Id = -1,
                    Name = "The simple one",
                    BasePrice = 2.12m,
                    CheeseCrust = false,
                    IsAvailable = true
                },
                new Pizza
                {
                    Id = -2,
                    Name = "The simple one premium",
                    BasePrice = 3.12m,
                    CheeseCrust = true,
                    IsAvailable = true
                },
                new Pizza
                {
                    Id = -3,
                    Name = "The all in",
                    BasePrice = 5.18m,
                    CheeseCrust = true,
                    IsAvailable = true
                }
            });
        }
    }
}
