﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PizzaPlace.Core.Entities;

namespace PizzaPlace.Infrastructure.Configurations
{
    public class PizzaOrderConfiguration : IEntityTypeConfiguration<PizzaOrder>
    {
        public void Configure(EntityTypeBuilder<PizzaOrder> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasMany(x => x.ExtraIngredients)
                .WithMany(i => i.PizzaOrders)
                .UsingEntity<Dictionary<string, object>>(
                    "PizzaIngredient",
                    r => r.HasOne<Ingredient>().WithMany().HasForeignKey("IngredientId"),
                    l => l.HasOne<PizzaOrder>().WithMany().HasForeignKey("PizzaOrdersId"),
                    rl =>
                    {
                        rl.HasKey("IngredientId", "PizzaOrdersId");
                        rl.HasData(
                            new { PizzaOrdersId = -1, IngredientId = -6 },
                            new { PizzaOrdersId = -1, IngredientId = -2 });
                    });

            builder.HasData(new List<PizzaOrder>
            {
                new PizzaOrder
                {
                    PizzaId = -1,
                    OrderId = -1,
                    Quantity = 1,
                    Id = -1,
                    Total = 2.88m,
                    ExtrasTotal = 0.76m
                },
                new PizzaOrder
                {
                    PizzaId = -3,
                    OrderId = -1,
                    Quantity = 1,
                    Id = -2,
                    Total = 5.18m,
                    ExtrasTotal = 0m
                }
            });
        }
    }
}
