﻿using System;
using Microsoft.EntityFrameworkCore;
using PizzaPlace.Core.Entities;
using PizzaPlace.Infrastructure.Configurations;

namespace PizzaPlace.Infrastructure
{
    public class PizzaPlaceDbContext : DbContext
    {
        public PizzaPlaceDbContext(DbContextOptions<PizzaPlaceDbContext> options)
            : base(options)
        {
            
        }

        public DbSet<Pizza> Pizzas { get; set; }

        public DbSet<Ingredient> Ingredients { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<PizzaOrder> PizzaOrders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PizzaConfiguration());
            modelBuilder.ApplyConfiguration(new IngredientConfiguration());
            modelBuilder.ApplyConfiguration(new PizzaOrderConfiguration());
            modelBuilder.ApplyConfiguration(new OrderConfiguration());
        }
    }
}
