﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PizzaPlace.Core.Interfaces;

namespace PizzaPlace.Infrastructure.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected readonly PizzaPlaceDbContext Context;

        public BaseRepository(PizzaPlaceDbContext context)
        {
            Context = context;
        }

        public async Task<IReadOnlyList<TEntity>> GetAllWithRelatedDataAsync(params Expression<Func<TEntity, object>>[] includes)
        {
            return await Include(includes).AsNoTracking().ToListAsync();
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            var newEntity = await Context.AddAsync(entity);
            await Context.SaveChangesAsync();
            return newEntity.Entity;
        }

        public async Task<TEntity> FirstOrDefaultWithRelatedDataAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            return await Include(includes).FirstOrDefaultAsync(predicate);
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            var entry = Context.Entry(entity);
            entry.State = EntityState.Modified;
            await Context.SaveChangesAsync();
            return entity;
        }

        private IQueryable<TEntity> Include(params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            return includes.Aggregate(query, (current, include) => current.Include(include));
        }
    }
}
