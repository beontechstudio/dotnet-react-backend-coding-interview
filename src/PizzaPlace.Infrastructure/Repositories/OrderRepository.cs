﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PizzaPlace.Core.Entities;
using PizzaPlace.Core.Interfaces;

namespace PizzaPlace.Infrastructure.Repositories
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(PizzaPlaceDbContext context)
            : base(context)
        {
        }

        public async Task<IReadOnlyList<Order>> GetOrdersAsync()
        {
            var orders = Context.Orders.Include(x => x.Pizzas)
                .ThenInclude(x => x.Pizza)
                .Include(x => x.Pizzas)
                .ThenInclude(x => x.ExtraIngredients)
                .AsNoTracking();
            return await orders.ToListAsync();
        }

        public Task<Order> GetOrderByIdAsync(int orderId)
        {
            return Context.Orders.Include(x => x.Pizzas)
                .ThenInclude(x => x.Pizza)
                .Include(x => x.Pizzas)
                .ThenInclude(x => x.ExtraIngredients)
                .FirstOrDefaultAsync(x => x.Id == orderId);
        }

        public async Task<Order> AddOrderAsync(Order order)
        {
            Context.AttachRange(order.Pizzas.SelectMany(x => x.ExtraIngredients));
            await AddAsync(order);
            return await GetOrderByIdAsync(order.Id);
        }
    }
}
